from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
from django.urls import reverse_lazy
from .models import Elemento, TipoMoneda
from .forms import ElementoForm

class ElementoListView(ListView):
    model = Elemento
    context_object_name = 'elemento'

class ElementoCreateView(CreateView):
    model = Elemento
    form_class = ElementoForm
    success_url = reverse_lazy('elemento_changelist')

class ElementoUpdateView(UpdateView):
    model = Elemento
    form_class = ElementoForm
    success_url = reverse_lazy('elemento_changelist')

def cargar_tipo_moneda(request):
    pais_id = request.GET.get('pais')
    tipos_moneda = Pais.objects.filter(pais_id=pais_id).order_by('nombre')
    return render(request, 'tipo_moneda.html', {'tipos_moneda': tipos_moneda})