from django.db import models

# Create your models here.

class Pais(models.Model):
    nombre = models.CharField(max_length=30)


class TipoMoneda(models.Model):
    nombre = models.CharField(max_length=30)

    # Relaciones foráneas (llaves foráneas)
    id_pais = models.ForeignKey(Pais, on_delete=models.CASCADE)

class Elemento(models.Model):

    denominacion=models.DecimalField(max_digits=7, decimal_places=3 )

    #0 para monedas y 1 para billetes
    tipo=models.BooleanField(default=True)

    foto = models.ImageField(upload_to='imagenes/', default='imagenes/ninguna.jpg')

    # Relaciones foráneas (llaves foráneas)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    tipo_moneda = models.ForeignKey(TipoMoneda, on_delete=models.CASCADE)